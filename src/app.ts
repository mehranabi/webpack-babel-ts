export const run = (message: string) => {
    const app = document.getElementById('app')

    const div = document.createElement('div')
    div.innerText = message

    setTimeout(() => {
        app.replaceWith(div)
    }, 1000)
}